FROM node:12.16.0 as base

FROM base as builder-prepare

RUN mkdir -p /opt/builder
WORKDIR /opt/builder
